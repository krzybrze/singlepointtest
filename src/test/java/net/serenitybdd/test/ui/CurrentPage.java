package net.serenitybdd.test.ui;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

public class CurrentPage extends PageObject {

  @FindBy(xpath = "//*[@id=\"hdtb-msb-vis\"]/div[1]")
  private WebElementFacade navigationTab;

  public String getTextFromContent() {
    return navigationTab.getText();
  }
}
