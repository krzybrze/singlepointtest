package net.serenitybdd.test.ui;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.Keys;

@DefaultUrl("https://google.pl")
public class HomePage extends PageObject {

  @FindBy(xpath = "//*[@id=\"tsf\"]/div[2]/div/div[1]/div/div[1]/input")
  private WebElementFacade searchBox;

  public void typeSearchPhrase(String code) {
    searchBox.type(code);
    searchBox.sendKeys(Keys.ENTER);
  }
}
