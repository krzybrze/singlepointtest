package net.serenitybdd.test.feature.navigation;

import static net.serenitybdd.rest.SerenityRest.given;
import static org.assertj.core.api.Assertions.assertThat;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import java.util.Map.Entry;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.test.model.Exchange;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(SerenityRunner.class)
public class ApiTest {

  private static final String GET_URL = "https://api.exchangeratesapi.io/latest";

  @Test
  public void get200Response() {
    Response response = response(GET_URL);
    response.then().log().all();
    assertThat(response.getStatusCode()).isEqualTo(200);
  }

  @Test
  public void convertEurToUsd() {
    double valueToConvert = 20;
    Response response = response(GET_URL);
    Exchange exchange = response.then().extract().as(Exchange.class);
    System.out.println("USD: " + exchange.getRates().get("USD") * valueToConvert);
  }

  @Test
  public void highestExchange() {
    Response response = response(GET_URL);
    Exchange exchange = response.then().extract().as(Exchange.class);
    Entry<String, Double> max = exchange.getRates().entrySet().stream()
        .max((entry1, entry2) -> entry1.getValue() > entry2.getValue() ? 1 : -1).get();
    System.out.println(max);
  }

  private static Response response(String url) {
    return given()
        .contentType(ContentType.JSON)
        .get(url);
  }
}
