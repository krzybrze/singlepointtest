package net.serenitybdd.test.feature.navigation;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.test.steps.NavigatingUser;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityRunner.class)
public class UiTest {

  @Steps
  private NavigatingUser user;

  @Managed
  WebDriver browser;

  @Title("User should see the correct content when performed search")
  @Test
  public void shouldSeeTheContentTitleWhenPerformedSearch() {
    // Given
    user.isOnTheHomePage();
    // When
    user.performSearch("test");
    // Then
    user.shouldSeeTheCorrectContent("Wszystko");
  }
}
