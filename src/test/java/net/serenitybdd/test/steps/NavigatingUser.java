package net.serenitybdd.test.steps;

import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;
import static org.assertj.core.api.Assertions.assertThat;

import net.serenitybdd.test.ui.CurrentPage;
import net.serenitybdd.test.ui.HomePage;
import net.thucydides.core.annotations.Step;

public class NavigatingUser {

  private HomePage homePage;
  private CurrentPage currentPage;

  @Step
  public void isOnTheHomePage() {
    homePage.open();
    getDriver().manage().window().maximize();
  }

  @Step
  public void shouldSeeTheCorrectContent(String expectedTitle) {
    assertThat(currentPage.getTextFromContent()).isEqualTo(expectedTitle);
  }

  @Step
  public void performSearch(String phrase) {
    homePage.typeSearchPhrase(phrase);
  }
}
