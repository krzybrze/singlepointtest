package net.serenitybdd.test.model;

import java.util.Date;
import java.util.HashMap;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Exchange {

  private String base;
  private HashMap<String, Double> rates = new HashMap<>();
  private Date date;
}
